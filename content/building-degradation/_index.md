---
title: "Building Degradation"
---

## Condition Formula

`Condition = 0.67 / (1 + e^((1789/25000) * (DaysSinceLastRepair - C))) + 0.33`

**C** is either `100.87` or `107.87`.

There is speculation that the additional 7 days for **C** is a bug as it happens inconsistently.

## Repair costs and reclaimables

Repair cost is determined by:

`RepairCost = BuildingCost - ReclaimableCost`

ReclaimableCost is determined by:

`ReclaimableCost = Floor(BuildingCost * ((180 - DaysSinceLastRepair) / 180))`

- All of the above is applied on a **per material** basis.  
- If your building is affected by the 7day bug, subtract 7 from DaysSinceLastRepair.  If DaysSinceLastRepair goes negative, set it to `0`.
